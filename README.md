[ ![Codeship Status for lucasfsousa/ac-java-test](https://codeship.com/projects/aa8c0350-0d99-0134-66c7-062bb2cb9dbf/status?branch=master)](https://codeship.com/projects/156179)

# AC - Java Coding Test #

## Testing ##
To execute Java tests use the following command:

```
mvn test
```

### Testing using Postman ###
If you wanna test with Postman, you can import an existing collection:

```
ac-java-teste.postman_collection.json
```

## Running ##
To run the project use the following command:

```
mvn spring-boot:run
```

## Endpoints ##

- POST /script - Process a movie script
- GET /settings - Returns information about all the movie settings
- GET /settings/{id} - Returns information about the movie setting with the given id
- GET /characters - Returns information about all the movie characters
- GET /characters/{id} - Returns information about the movie character with the given id

## Heroku ##
The application was deployed on Heroku and can be accessed in:
```
https://ac-java-test.herokuapp.com
```

## Libraries: ##
- Spring Boot
- Spring MVC
- Spring Data JPA
- HSQLDB
- Swagger