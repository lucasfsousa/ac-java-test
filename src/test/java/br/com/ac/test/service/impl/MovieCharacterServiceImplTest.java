package br.com.ac.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.ac.test.domain.MovieCharacter;
import br.com.ac.test.exception.ResourceNotFoundException;
import br.com.ac.test.repository.MovieCharacterRepository;

public class MovieCharacterServiceImplTest {
	@Mock
	private MovieCharacterRepository repository;
	
	@InjectMocks
	private MovieCharacterServiceImpl service = new MovieCharacterServiceImpl();
	
	public MovieCharacterServiceImplTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void mustThrowExceptionGettingCharacterNotFound(){
		try {
			service.getCharacter(100L);
			fail();
		} catch (ResourceNotFoundException e) {
			assertEquals("Movie character with id 100 not found", e.getMessage());
		}
		
		verify(repository).findOne(100L);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void mustReturnExistingCharacter(){
		Long id = 123L;
		MovieCharacter character = new MovieCharacter();
		when(repository.findOne(id)).thenReturn(character );
		
		assertEquals(character, service.getCharacter(id));
		
		verify(repository).findOne(id);
		verifyNoMoreInteractions(repository);
	}
}
