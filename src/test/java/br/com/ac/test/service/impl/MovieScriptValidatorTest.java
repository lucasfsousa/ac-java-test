package br.com.ac.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.com.ac.test.service.impl.MovieScriptValidator;

public class MovieScriptValidatorTest {
	@Test
	public void mustHaveAValidSettingName(){
		assertHasSettingAndText("INT. REBEL BLOCKADE RUNNER - MAIN PASSAGEWAY", "REBEL BLOCKADE RUNNER");
		assertHasSettingAndText("EXT. TATOOINE - ANCHORHEAD SETTLEMENT - POWER STATION - DAY", "TATOOINE");
		assertHasSettingAndText("INT./EXT. LUKE'S SPEEDER - DESERT WASTELAND - TRAVELING - DAY", "LUKE'S SPEEDER");
	}
	
	@Test
	public void mustHaveAValidCharacterName(){
		assertHasCharacterAndText("                      LUKE", "LUKE");
		assertHasCharacterAndText("                      DETENTION CORRIDOR", "DETENTION CORRIDOR");
		assertHasCharacterAndText("                      MASSASSI INTERCOM VOICE", "MASSASSI INTERCOM VOICE");
	}
	
	@Test
	public void mustHaveAValidDialog(){
		assertHasDialogAndText("          I'd say about twenty guns. Some on", "I'd say about twenty guns. Some on");
		assertHasDialogAndText("          minutes.", "minutes.");
		assertHasDialogAndText("          The guns... they've stopped!", "The guns... they've stopped!");
		
	}
	
	private void assertHasSettingAndText(String line, String expected){
		MovieScriptValidator validator = new MovieScriptValidator(line);
		
		assertTrue(validator.hasSettingName());
		assertFalse(validator.hasCharacterName());
		assertFalse(validator.hasDialog());
		assertEquals(expected, validator.getSettingName());
	}
	
	private void assertHasCharacterAndText(String line, String expected){
		MovieScriptValidator validator = new MovieScriptValidator(line);
		
		assertTrue(validator.hasCharacterName());
		assertFalse(validator.hasSettingName());
		assertFalse(validator.hasDialog());
		assertEquals(expected, validator.getCharacterName());
	}
	
	private void assertHasDialogAndText(String line, String expected){
		MovieScriptValidator validator = new MovieScriptValidator(line);
		
		assertTrue(validator.hasDialog());
		assertFalse(validator.hasCharacterName());
		assertFalse(validator.hasSettingName());
		assertEquals(expected, validator.getDialog());
	}
}
