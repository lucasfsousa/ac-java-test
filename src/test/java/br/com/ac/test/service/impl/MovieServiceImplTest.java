package br.com.ac.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import br.com.ac.test.domain.MovieCharacter;
import br.com.ac.test.domain.MovieSetting;
import br.com.ac.test.exception.MovieScriptAlreadyReceivedException;
import br.com.ac.test.repository.MovieCharacterRepository;
import br.com.ac.test.repository.MovieSettingRepository;
import br.com.ac.test.vo.MovieScriptData;

public class MovieServiceImplTest {
	@Mock
	private MovieCharacterRepository characterRepository;
	
	@Mock
	private MovieSettingRepository settingRepository;
	
	@Spy
	@InjectMocks
	private MovieServiceImpl service = new MovieServiceImpl();
	
	public MovieServiceImplTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void mustThrowAnExceptionProcessMovieScriptAlreadyReceived(){
		String movieScript = "123";
		
		when(settingRepository.count()).thenReturn(1L);
		try {
			service.processMovieScript(movieScript);
			fail();
		} catch (MovieScriptAlreadyReceivedException e) {
			
		}
		
		verify(settingRepository).count();
		verifyNoMoreInteractions(settingRepository);
		verifyNoMoreInteractions(characterRepository);
		
		verify(service).processMovieScript(movieScript);
		verifyNoMoreInteractions(service);
	}
	
	@Test
	public void mustRunOkAllFunctionsIfAMovieWasntReceived(){
		String movieScript = "123";
		MovieScriptData scriptData = new MovieScriptData();
		
		when(settingRepository.count()).thenReturn(0L);
		doReturn(scriptData).when(service).readMovieScript(movieScript);
		doNothing().when(service).saveCharactersAndWords(scriptData);
		doNothing().when(service).saveSettings(scriptData);
		
		service.processMovieScript(movieScript);
		
		verify(settingRepository).count();
		verifyNoMoreInteractions(settingRepository);
		verifyNoMoreInteractions(characterRepository);
		
		verify(service).processMovieScript(movieScript);
		verify(service).readMovieScript(movieScript);
		verify(service).saveCharactersAndWords(scriptData);
		verify(service).saveSettings(scriptData);
		verifyNoMoreInteractions(service);
	}
	
	@Test
	public void readMovieScript(){
		String movieScript = 
		"INT. REBEL BLOCKADE RUNNER\n" +
		"Threepio stands in a hallway, somewhat bewildered. Artoo is\n" +
		"nowhere in sight. The pitiful screams of the doomed Rebel\n" +
		"soldiers can be heard in the distance.\n" +
		"\n" +
		"                      THREEPIO\n" +
		"          Artoo! Artoo-Detoo, where are you?";
		
		MovieScriptData scriptData = service.readMovieScript(movieScript);
		
		assertEquals(1, scriptData.getCharactersNames().size());
		assertEquals("THREEPIO", scriptData.getCharactersNames().iterator().next());
		assertEquals(1, scriptData.getSettingsNames().size());
		assertEquals("REBEL BLOCKADE RUNNER", scriptData.getSettingsNames().iterator().next());
		assertEquals(5, scriptData.getWords("THREEPIO").size());
	}
	
	@Test
	public void saveCharactersAndWords(){
		MovieScriptData scriptData = new MovieScriptData();
		//create 2 settings
		scriptData.addSetting("setting 01");
		scriptData.addSetting("setting 02");
		
		//associate 2 character to the settings
		scriptData.addCharacter("setting 01", "character 01");
		scriptData.addCharacter("setting 02", "character 01");
		scriptData.addCharacter("setting 01", "character 02");
		
		//associate dialog to character
		scriptData.addDialog("character 02", "Anthony Collins is a great guy");
		scriptData.addDialog("character 02", "isn't it?");
		
		service.saveCharactersAndWords(scriptData);
		
		verify(characterRepository, times(2)).save(any(MovieCharacter.class));
		verifyNoMoreInteractions(characterRepository);
	}
	
	@Test
	public void saveSettings(){
		MovieScriptData scriptData = new MovieScriptData();
		//create 3 settings
		scriptData.addSetting("setting 01");
		scriptData.addSetting("setting 02");
		scriptData.addSetting("setting 03");
		
		//associate 2 character to the settings
		scriptData.addCharacter("setting 01", "character 01");
		scriptData.addCharacter("setting 01", "character 02");
		
		service.saveSettings(scriptData);
		
		verify(settingRepository, times(3)).save(any(MovieSetting.class));
		verifyNoMoreInteractions(settingRepository);
		
		verify(characterRepository, times(2)).findByName(anyString());
		verifyNoMoreInteractions(characterRepository);
		
	}
}
