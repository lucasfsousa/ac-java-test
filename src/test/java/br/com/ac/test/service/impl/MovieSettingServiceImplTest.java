package br.com.ac.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.ac.test.domain.MovieSetting;
import br.com.ac.test.exception.ResourceNotFoundException;
import br.com.ac.test.repository.MovieSettingRepository;

public class MovieSettingServiceImplTest {
	@Mock
	private MovieSettingRepository repository;
	
	@InjectMocks
	private MovieSettingServiceImpl service = new MovieSettingServiceImpl();
	
	public MovieSettingServiceImplTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void mustThrowExceptionGettingSettingNotFound(){
		try {
			service.getSetting(100L);
			fail();
		} catch (ResourceNotFoundException e) {
			assertEquals("Movie setting with id 100 not found", e.getMessage());
		}
		
		verify(repository).findOne(100L);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void mustReturnExistingCharacter(){
		Long id = 123L;
		MovieSetting setting = new MovieSetting();
		when(repository.findOne(id)).thenReturn(setting);
		
		assertEquals(setting, service.getSetting(id));
		
		verify(repository).findOne(id);
		verifyNoMoreInteractions(repository);
	}
}
