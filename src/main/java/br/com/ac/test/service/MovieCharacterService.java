package br.com.ac.test.service;

import java.util.List;

import br.com.ac.test.domain.MovieCharacter;

public interface MovieCharacterService {
	List<MovieCharacter> getCharacters();
	MovieCharacter getCharacter(Long id);
}
