package br.com.ac.test.service;

import java.util.List;

import br.com.ac.test.domain.MovieSetting;

public interface MovieSettingService {
	List<MovieSetting> getSettings();
	MovieSetting getSetting(Long id);
}
