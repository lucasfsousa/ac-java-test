package br.com.ac.test.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ac.test.domain.MovieCharacter;
import br.com.ac.test.exception.ResourceNotFoundException;
import br.com.ac.test.repository.MovieCharacterRepository;
import br.com.ac.test.service.MovieCharacterService;

@Service
public class MovieCharacterServiceImpl implements MovieCharacterService {
	@Autowired
	private MovieCharacterRepository repository;
	
	private Logger log = Logger.getLogger(MovieCharacterServiceImpl.class);
	
	@Override
	public List<MovieCharacter> getCharacters() {
		log.info("Finding movie characters");
		return repository.findAll();
	}

	@Override
	public MovieCharacter getCharacter(Long id) {
		log.info("Finding movie character: " + id);
		MovieCharacter movieCharacter = repository.findOne(id);
		if(movieCharacter == null){
			log.error("Movie character with id " + id + " not found");
			throw new ResourceNotFoundException("Movie character with id " + id + " not found");
		}
		return movieCharacter;
	}
}
