package br.com.ac.test.service;

public interface MovieService {
	void processMovieScript(String movieScript);
}
