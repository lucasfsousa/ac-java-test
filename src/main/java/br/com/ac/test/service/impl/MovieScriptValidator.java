package br.com.ac.test.service.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MovieScriptValidator {
	private static final Pattern REGEX_SETTING_NAME = Pattern.compile("^(?:INT\\.\\/EXT.|INT\\.|EXT\\.)\\s([^-\\s]+(\\s[^-\\s]+)*).*$");
	private static final Pattern REGEX_CHARACTER_NAME = Pattern.compile("^\\s{22}([A-Z]+(\\s[A-Z]+)*)$");
	private static final Pattern REGEX_DIALOG = Pattern.compile("^\\s{10}([\\S]+(\\s[\\S]+)*)$");
	
	private String settingName;
	private String characterName;
	private String dialog;
	
	public MovieScriptValidator(String line){
		processSettingName(line);
		processCharacterName(line);
		processDialog(line);
	}

	public boolean hasSettingName(){
		return getSettingName() != null;
	}
	
	public boolean hasCharacterName(){
		return getCharacterName() != null;
	}
	
	public boolean hasDialog(){
		return getDialog() != null;
	}
	
	public String getSettingName(){
		return settingName;
	}
	
	public String getCharacterName(){
		return characterName;
	}
	
	public String getDialog(){
		return dialog;
	}

	private void processSettingName(String line) {
		Matcher matcher = REGEX_SETTING_NAME.matcher(line);
		if(matcher.matches()){
			settingName = matcher.group(1);
		}
	}
	
	private void processCharacterName(String line) {
		Matcher matcher = REGEX_CHARACTER_NAME.matcher(line);
		if(matcher.matches()){
			characterName = matcher.group(1);
		}
	}
	
	private void processDialog(String line) {
		Matcher matcher = REGEX_DIALOG.matcher(line);
		if(matcher.matches()){
			dialog = matcher.group(1);
		}
	}
}
