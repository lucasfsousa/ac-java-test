package br.com.ac.test.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ac.test.domain.MovieSetting;
import br.com.ac.test.exception.ResourceNotFoundException;
import br.com.ac.test.repository.MovieSettingRepository;
import br.com.ac.test.service.MovieSettingService;

@Service
public class MovieSettingServiceImpl implements MovieSettingService {
	@Autowired
	private MovieSettingRepository repository;
	
	private Logger log = Logger.getLogger(MovieSettingServiceImpl.class);
	
	@Override
	public List<MovieSetting> getSettings() {
		log.info("Finding movie settings");
		return repository.findAll();
	}

	@Override
	public MovieSetting getSetting(Long id) {
		log.info("Finding movie setting: " + id);
		
		MovieSetting movieSetting = repository.findOne(id);
		if(movieSetting == null){
			log.error("Movie seting with id " + id + " not found");
			throw new ResourceNotFoundException("Movie setting with id " + id + " not found");
		}
		return movieSetting;
	}
}
