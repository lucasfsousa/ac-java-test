package br.com.ac.test.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ac.test.domain.MovieCharacter;
import br.com.ac.test.domain.MovieSetting;
import br.com.ac.test.domain.WordCount;
import br.com.ac.test.exception.MovieScriptAlreadyReceivedException;
import br.com.ac.test.repository.MovieCharacterRepository;
import br.com.ac.test.repository.MovieSettingRepository;
import br.com.ac.test.service.MovieService;
import br.com.ac.test.vo.MovieScriptData;

@Service
public class MovieServiceImpl implements MovieService {
	
	@Autowired
	private MovieCharacterRepository characterRepository;
	
	@Autowired
	private MovieSettingRepository settingRepository;
	
	private Logger log = Logger.getLogger(MovieServiceImpl.class);
	
	@Override
	public void processMovieScript(String movieScript) {
		log.info("Movie script received");
		
		//if a movie script was already received before, throw an Exception
		if(settingRepository.count() > 0){
			log.error("Movie script already received");
			throw new MovieScriptAlreadyReceivedException();
		}
		
		//Read the movie script and return object that represents the data
		MovieScriptData scriptData = readMovieScript(movieScript);
		
		//save characters and their words
		log.info("Saving characters and their words");
		saveCharactersAndWords(scriptData);
		
		//save settings and their characters
		log.info("Saving settings and their words");
		saveSettings(scriptData);
	}
	
	protected MovieScriptData readMovieScript(String movieScript) {
		MovieScriptData movieScriptData = new MovieScriptData();
		
		//Open a scanner to read string each line of string
		Scanner scanner = new Scanner(movieScript);
		
		//Temp objects used in reading process
		String lastSetting = null;
		String lastCharacter = null;

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			MovieScriptValidator validator = new MovieScriptValidator(line);

			if (validator.hasSettingName()) {
				lastSetting = validator.getSettingName();
				//add setting
				movieScriptData.addSetting(lastSetting);
			} else if (validator.hasCharacterName()) {
				lastCharacter = validator.getCharacterName();
				//add character to setting
				movieScriptData.addCharacter(lastSetting, lastCharacter);
			} else if (validator.hasDialog()) {
				String lastDialog = validator.getDialog();
				movieScriptData.addDialog(lastCharacter, lastDialog);
			}
		}
		
		scanner.close();
		
		return movieScriptData;
	}
	
	protected void saveCharactersAndWords(MovieScriptData scriptData) {
		//get characters names and their words to save
		for(String characterName : scriptData.getCharactersNames()){
			MovieCharacter character = new MovieCharacter();
			character.setName(characterName);

			List<WordCount> listWords = new ArrayList<>();
			//find each word of character
			for(String word : scriptData.getWords(characterName)){
				WordCount wordCount = new WordCount();
				wordCount.setWord(word);
				wordCount.setCount(scriptData.getWordCount(characterName, word));
				
				listWords.add(wordCount);
			}
			
			character.setWordCounts(listWords);
			
			log.info("Saving character " + characterName);
			characterRepository.save(character);
		}
	}
	
	protected void saveSettings(MovieScriptData scriptData) {
		//get settings names and their words to save
		for(String settingName : scriptData.getSettingsNames()){
			MovieSetting movieSetting = new MovieSetting();
			movieSetting.setName(settingName);
			
			List<MovieCharacter> listCharacters = new ArrayList<MovieCharacter>();
			for(String characterName : scriptData.getCharactersNames(settingName)){
				//find the character object in the DB
				listCharacters.add(characterRepository.findByName(characterName));
			}
			movieSetting.setCharacters(listCharacters);
			
			log.info("Saving setting " + settingName);
			settingRepository.save(movieSetting);
		}
	}
}
