package br.com.ac.test.vo;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class MovieScriptData {
	Map<String, Set<String>> settingsAndCharacters = new LinkedHashMap<String, Set<String>>();
	Map<String, Map<String, Integer>> charactersDialogWordCount = new LinkedHashMap<String, Map<String, Integer>>();

	public void addSetting(String settingName) {
		if (!settingsAndCharacters.containsKey(settingName)) {
			settingsAndCharacters.put(settingName, new LinkedHashSet<String>());
		}
	}

	public void addCharacter(String settingName, String characterName) {
		settingsAndCharacters.get(settingName).add(characterName);

		// if the key is not present in the map, add it with an empty List
		if (!charactersDialogWordCount.containsKey(characterName)) {
			charactersDialogWordCount.put(characterName, new LinkedHashMap<String, Integer>());
		}
	}

	public void addDialog(String characterName, String dialog) {
		for (String word : dialog.split(" ")) {
			//remove punctuation and convert the word to lowercase
			word = word.replaceAll("[.!?,]", "").toLowerCase();
			
			if(!charactersDialogWordCount.get(characterName).containsKey(word)){
				charactersDialogWordCount.get(characterName).put(word, 1);
			}
			else{
				charactersDialogWordCount.get(characterName).put(word, charactersDialogWordCount.get(characterName).get(word) + 1);
			}
		}
	}
	
	public Set<String> getCharactersNames() {
		return charactersDialogWordCount.keySet();
	}

	public Set<String> getWords(String characterName) {
		return charactersDialogWordCount.get(characterName).keySet();
	}

	public Integer getWordCount(String characterName, String word) {
		return charactersDialogWordCount.get(characterName).get(word);
	}

	public Set<String> getSettingsNames() {
		return settingsAndCharacters.keySet();
	}

	public Set<String> getCharactersNames(String settingName) {
		return settingsAndCharacters.get(settingName);
	}
}
