package br.com.ac.test.vo;

public class MessageVO {
	private String message;

	public MessageVO(String message) {
		super();
		this.message = message;
	}

	public MessageVO() {
		super();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
