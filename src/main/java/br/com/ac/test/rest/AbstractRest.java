package br.com.ac.test.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractRest {
	protected <T> ResponseEntity<T> responseOK(T body){
		return new ResponseEntity<T>(body, HttpStatus.OK);
	}
}
