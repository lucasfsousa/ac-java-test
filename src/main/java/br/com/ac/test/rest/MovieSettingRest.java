package br.com.ac.test.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ac.test.domain.MovieSetting;
import br.com.ac.test.service.MovieSettingService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/settings")
@Api(tags={"Settings"})
public class MovieSettingRest extends AbstractRest {
	@Autowired
	private MovieSettingService service;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<MovieSetting>> getSettings(){
		return responseOK(service.getSettings());
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<MovieSetting> getSetting(@PathVariable("id") Long id){
		return responseOK(service.getSetting(id));
	}
}
