package br.com.ac.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ac.test.service.MovieService;
import br.com.ac.test.vo.MessageVO;
import io.swagger.annotations.Api;

@RestController
@Api(tags={"Script"})
public class MovieRest extends AbstractRest {
	@Autowired
	private MovieService service;
	
	@RequestMapping(value = "/script", method = RequestMethod.POST)
	public ResponseEntity<MessageVO> postScript(@RequestBody String movieScript){
		service.processMovieScript(movieScript);
		return responseOK(new MessageVO("Movie script successfully received"));
	}
}