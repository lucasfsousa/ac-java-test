package br.com.ac.test.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ac.test.domain.MovieCharacter;
import br.com.ac.test.service.MovieCharacterService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/characters")
@Api(tags={"Characters"})
public class MovieCharacterRest extends AbstractRest {
	@Autowired
	private MovieCharacterService service;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<MovieCharacter>> getCharacters(){
		return responseOK(service.getCharacters());
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<MovieCharacter> getCharacter(@PathVariable("id") Long id){
		return responseOK(service.getCharacter(id));
	}
}
