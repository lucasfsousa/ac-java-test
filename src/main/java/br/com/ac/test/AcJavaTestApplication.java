package br.com.ac.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcJavaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcJavaTestApplication.class, args);
	}
}
