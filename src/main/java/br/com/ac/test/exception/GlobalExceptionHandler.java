package br.com.ac.test.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.ac.test.vo.MessageVO;

@ControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(MovieScriptAlreadyReceivedException.class)
	public ResponseEntity<MessageVO> businessException(MovieScriptAlreadyReceivedException e){
		return new ResponseEntity<MessageVO>(new MessageVO("Movie script already received"), HttpStatus.FORBIDDEN);
	}
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<MessageVO> businessException(ResourceNotFoundException e){
		return new ResponseEntity<MessageVO>(new MessageVO(e.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<MessageVO> genericError(MovieScriptAlreadyReceivedException e){
		return new ResponseEntity<MessageVO>(new MessageVO("Unexpected error"), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
