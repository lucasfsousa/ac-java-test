package br.com.ac.test.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "WORDS_COUNT")
public class WordCount {
	@Id
	@GeneratedValue
	@JsonIgnore
	private Long id;

	private String word;

	@Column(name = "qty")
	private Integer count;

	public WordCount() {
		super();
	}

	public WordCount(String word, Integer count) {
		super();
		this.word = word;
		this.count = count;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
