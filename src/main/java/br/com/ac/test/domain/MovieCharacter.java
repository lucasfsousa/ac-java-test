package br.com.ac.test.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name="CHARACTERS")
public class MovieCharacter {
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(unique = true)
	private String name;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="character_id")
	@OrderBy("count desc")
	private List<WordCount> wordCounts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WordCount> getWordCounts() {
		return wordCounts;
	}

	public void setWordCounts(List<WordCount> wordCounts) {
		this.wordCounts = wordCounts;
	}

}
