package br.com.ac.test.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="MOVIE_SETTINGS")
public class MovieSetting {
	@Id
	@GeneratedValue
	private Long id;
	
	private String name;
	
	@ManyToMany
	@JoinTable(name="SETTINGS_CHARACTERS",
		joinColumns=@JoinColumn(name="setting_id", referencedColumnName="id")
		, inverseJoinColumns=@JoinColumn(name="character_id", referencedColumnName="id")
	)
	private List<MovieCharacter> characters;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MovieCharacter> getCharacters() {
		return characters;
	}

	public void setCharacters(List<MovieCharacter> characters) {
		this.characters = characters;
	}

}
