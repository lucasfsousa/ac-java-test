package br.com.ac.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	@RequestMapping("/")
    public String home() {
		//Redirect to Swagger
        return "redirect:swagger-ui.html";
    }
}
