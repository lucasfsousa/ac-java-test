package br.com.ac.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ac.test.domain.MovieCharacter;

public interface MovieCharacterRepository extends JpaRepository<MovieCharacter, Long> {
	public MovieCharacter findByName(String name);
}
