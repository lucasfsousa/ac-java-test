package br.com.ac.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ac.test.domain.MovieSetting;

public interface MovieSettingRepository extends JpaRepository<MovieSetting, Long> {

}
